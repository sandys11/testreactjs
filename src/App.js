import React from "react";
import "./style.css";
import "bootstrap/dist/css/bootstrap.css";
import Dropdown from "react-bootstrap/Dropdown";
import { ArrowRight } from "react-bootstrap-icons";

export default function App() {
  return (
    <div id="body">
      <Header />
      <Card
        className="section"
        title="Who we are"
        text="Technology Company"
        description="Sed ut perspiciatis unde omnis iste natus sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo."
        numbering="01/03"
      />

      <Card
        className="section "
        title="What we do"
        text="Professional Brand Management"
        description="Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem."
        numbering="02/03"
      />

      <Card
        className="section "
        title="How we do"
        text="Strategize, Design, Collaborate"
        description="Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse sequam nihil molestiae consequatur."
        numbering="03/03"
      />
      <Card
        className="section"
        title="Our Core Values"
        text="Ridiculus laoreet libero pretium et, sit vel elementum convallis fames. Sit suspendisse etiam eget egestas. Aliquet odio et lectus etiam sit.

In mauris rutrum ac ut volutpat, ornare nibh diam. Montes, vitae, nec amet enim."
        img="./Capture1.PNG"
       name="Dedication"
       deskripsi="Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat."
      />

      <Footer />
    </div>
  );
}

const Header = () => {
  return (
    <div className="header">
      <span className="header-title">
        <h1
          style={{
            color: "4097DB",
            textAlign: "center",
            borderColor: "grey",
            borderRadius: "3px",
          }}
        >
          Company
        </h1>
      </span>
      
      <br />
  
    </div>
  );
};

const Card = (props) => {
  return (
    <div className={props.className}>
      <div className="small-div">
    
        <i className={props.className}></i>
        <i className={props.className}></i>
        <img src={props.img} alt="" />
     <br/>
     <i className={props.name}></i>
     <i className={props.deskripsi}></i>
      </div>

      <div className="big-div">
   
        <span className="div-title">{props.title}</span>
        <p
          className="div-title1"
          style={{ fontWeight: "bold", paddingTop: "5px" }}
        >
          {props.text}
        </p>

        <br />
        <span>{props.description}</span>
        <p className="div-page" style={{ paddingTop: "10px" }}>
          {props.numbering}
          <a href="/about" style={{ paddingLeft: "100%" }}>
            <button
              type="button"
              class="btn"
              style={{ borderColor: "grey", borderRadius: "3px" }}
            >
              <ArrowRight color="royalblue" size={16} />
            </button>
          </a>
        </p>
        <br />
       
      </div>
    </div>
  );
};


const Footer = () => {
  return (
    <div className="contact-container ">
      <img
        src="./download.jpg"
        className="App-logo"
        alt=""
        width="60"
        height="60"
      />
      <span className="div-title" style={{ paddingLeft: "60px" }}>
        COMPANY
      </span>
      <br />
      <div className="contact-form">
        <div id="sect1">
          <Dropdown style={{ textAlign: "left" }}>
            <Dropdown.Toggle variant="success">
              Technology Department
            </Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item href="#">Marketing</Dropdown.Item>
              <Dropdown.Item href="#">Finance</Dropdown.Item>
              <Dropdown.Item href="#">product</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>

          <span
            style={{
              textAlign: "justify",
              justifyContent: "left",
              paddingTop: "10px",
            }}
          >
            Jl. Lembong No 8 Kel. Braga Kec. Sumur Bandung, Kota Bandung, Jawa
            Barat
          </span>
        </div>
        <br />
      </div>
      <div className="contact-form1">
        <p>Who We Are</p>
        <p>Our Values</p>
        <p>The Perks</p>
      </div>
    </div>
  );
};
